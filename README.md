### Background ###

Jmeter is a Java tool that is used to perform performance testing. A script is executed in Jmeter that defines a series of requests that should be sent to the server. 
The response time of each request is logged and then a summary report is produced that defines the metrics (avergage, median ect) for each request.

For example, if the following requests were made to the server:

_Request 1 : Home Page, took 200ms_

_Request 2:  Home Page, took 400ms_

Then a "Summary" report would be produced containing a single line:

_Home Page, Count 2, Average 300ms_

An example of output file contains the following lines:

```
Label, Count, Average
Home_GotoLoginPage, 2, 500
Wf_ApproveAllAgmtOnApprovalsManager, 3, 100
```

### Build and Run ###

* Requirments
  
    * Java 1.8 
    * Maven 3.0 or above
  
* How to build and run.  
  
    * To run ready build
  
```
    //Open a terminal, and cd to the project folder
    cd build/
    java -jar target/log-info-extractor.jar Full.log
    more Summary.csv // view the report, assuming a Linux machine is in use   
  
```

    * To rebuild and then run

```
    //Open a terminal, and cd to the project folder
    mvn clean package
    java -jar target/log-info-extractor-1.0.0-SNAPSHOT-jar-with-dependencies.jar path_to_logFile.log
    more Summary.csv // view the report, assuming a Linux machine is in use
```
