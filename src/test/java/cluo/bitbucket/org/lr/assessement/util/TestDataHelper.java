package cluo.bitbucket.org.lr.assessement.util;


import cluo.bitbucket.org.lr.assessement.dto.InputRowData;
import cluo.bitbucket.org.lr.assessement.dto.OutputRowData;

public class TestDataHelper
{

    public static OutputRowData buildOutputRecord(final String pageLabel, final int count, final long avgLatency)
    {
        return OutputRowData.builder()
            .pageLabel(pageLabel)
            .count(count)
            .averageLatency(avgLatency).build();
    }

    public static InputRowData buildInputRecord(final String pageLabel, final long latency)
    {
        return InputRowData.builder()
            .latency(latency)
            .pageLabel(pageLabel).build();
    }

    public static String toCommaSeparatedString(String[] strings)
    {
        StringBuilder output = new StringBuilder();
        int loop = 0;
        int strCount = strings.length;

        for(String string : strings)
        {
            if(loop < strCount - 1)
            {
                output.append(string).append(",");
            }
            else
            {
                output.append(string);
            }
            loop += 1;
        }
        return output.toString();
    }
}
