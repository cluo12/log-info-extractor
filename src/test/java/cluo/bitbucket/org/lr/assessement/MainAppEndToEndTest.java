package cluo.bitbucket.org.lr.assessement;

import org.apache.commons.io.FileUtils;
import org.hamcrest.CoreMatchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static cluo.bitbucket.org.lr.assessement.util.TestDataHelper.toCommaSeparatedString;
import static junit.framework.TestCase.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;

public class MainAppEndToEndTest
{

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void summaryFileIsProduced() throws Exception
    {
         MainApp.main(new String[]{"src/test/resources/input/mini.log"});
         assertOutputFile();
         removeOutputFile();
    }

    @Test
    public void noSummaryFileProduced_invalidLogReceived() throws Exception {
        MainApp.main(new String[]{"src/test/resources/input/input-missing-label.log"});
        File summaryFile = new File(MainApp.OUTPUT_FILE);
        assertFalse(String.format("%s shall not be produced", MainApp.OUTPUT_FILE),summaryFile.exists());
    }

    private void assertOutputFile() throws IOException
    {
        List<String> outputRecords = FileUtils.readLines(new File(MainApp.OUTPUT_FILE));
        assertTrue("Number of rows in the outputfile", outputRecords.size() == 4);

        assertContainingRecord(outputRecords,"Home_GotoLoginPage", "2", "597");
        assertContainingRecord(outputRecords,"Home_Login", "2", "637");
        assertContainingRecord(outputRecords,"Home_GotoHomePage", "1", "494");
    }

    private void assertContainingRecord(List<String> records, String... fields)
    {
        assertThat("Record in the outputfile shall be matched",
                        records, CoreMatchers.hasItem(toCommaSeparatedString(fields)));
    }

    private void removeOutputFile()
    {
        File outputFile = new File(MainApp.OUTPUT_FILE);
        FileUtils.deleteQuietly(outputFile);
    }
}