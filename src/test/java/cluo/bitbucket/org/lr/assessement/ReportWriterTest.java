package cluo.bitbucket.org.lr.assessement;

import org.apache.commons.io.FileUtils;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cluo.bitbucket.org.lr.assessement.dto.OutputRowData;

import static cluo.bitbucket.org.lr.assessement.util.TestDataHelper.buildOutputRecord;
import static cluo.bitbucket.org.lr.assessement.util.TestDataHelper.toCommaSeparatedString;
import static org.hamcrest.core.StringContains.containsString;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

public class ReportWriterTest
{

    private static final String TEST_OUTPUT_FILE = "src/test/testoutput.csv";
    private ReportWriter reportWriter;

    @Before
    public void beforeEachTest() throws Exception {
        reportWriter = new ReportWriter(new File(TEST_OUTPUT_FILE));
    }

    @After
    public void afterEachTest() throws Exception {
        removeOutputFile();
    }

    @Test
    public void write_emptyOutputFileProduced_givenNoInputData() throws Exception
    {
        reportWriter.write(Collections.emptyList());
        List<String> outputFile = FileUtils.readLines(new File(TEST_OUTPUT_FILE));
        assertEquals("Number of lines is not right", 1, outputFile.size());
        assertThat("", outputFile.get(0),
             containsString(toCommaSeparatedString(ReportWriter.FILE_HEADERS)));
    }

    @Test
    public void write_outputFileProduced_givenCorrectInputData() throws Exception
    {
        reportWriter.write(buildOutputRecords());
        List<String> outPutfile = FileUtils.readLines(new File(TEST_OUTPUT_FILE));
        assertEquals("Number of lines is not right", 4, outPutfile.size());

        assertThat("The first record is not expected", outPutfile.get(1),
            containsString(toCommaSeparatedString(new String[]{"page1","3","123"})));
        assertThat("The second record is not expected", outPutfile.get(2),
            containsString(toCommaSeparatedString(new String[]{"page2","6","222"})));
        assertThat("The third record is not expected", outPutfile.get(3),
            containsString(toCommaSeparatedString(new String[]{"page3","10","777"})));
    }

    private List<OutputRowData> buildOutputRecords()
    {
        List<OutputRowData> records = new ArrayList<>();

        records.add(buildOutputRecord("page1", 3, 123L));
        records.add(buildOutputRecord("page2", 6, 222L));
        records.add(buildOutputRecord("page3", 10, 777L));

        return records;
    }

    private void removeOutputFile()
    {
        File outputFile = new File(TEST_OUTPUT_FILE);
        FileUtils.deleteQuietly(outputFile);
    }

}