package cluo.bitbucket.org.lr.assessement;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import cluo.bitbucket.org.lr.assessement.dto.InputRowData;
import cluo.bitbucket.org.lr.assessement.dto.OutputRowData;
import cluo.bitbucket.org.lr.assessement.util.TestDataHelper;

import static cluo.bitbucket.org.lr.assessement.util.TestDataHelper.buildInputRecord;
import static org.junit.Assert.assertTrue;

public class DefaultLogAnalyserTest
{
    private DefaultLogAnalyser analyser;

    @Before
    public void beforeEachTest() throws Exception {
       analyser = new DefaultLogAnalyser();
    }

    @Test
    public void analyse_returnEmptyResult_whenEmptyInputReceived() throws Exception
    {
        List<OutputRowData> output = analyser.analyse(Collections.emptyList());
        assertTrue("Output is expected to be an empty list", output.isEmpty());

    }

    @Test
    public void analyse_returnExpectedResult_whenValidInputReceived() throws Exception {
        List<InputRowData> inputRecords = new ArrayList<>();
        inputRecords.add(buildInputRecord("page1", 100L));
        inputRecords.add(buildInputRecord("page2", 200L));
        inputRecords.add(buildInputRecord("page1", 110L));
        inputRecords.add(buildInputRecord("page3", 300L));

        List<OutputRowData> actualOutputRecords = analyser.analyse(inputRecords);
        List<OutputRowData> expectedOutputRecords = buildOutputRecords();
        assertTrue("Two collections shall contain the same items ", actualOutputRecords.containsAll(expectedOutputRecords));
    }

    private List<OutputRowData> buildOutputRecords()
    {
        List<OutputRowData> output = new ArrayList<>();
        output.add(TestDataHelper.buildOutputRecord("page1",2, 105L));
        output.add(TestDataHelper.buildOutputRecord("page2",1, 200L));
        output.add(TestDataHelper.buildOutputRecord("page3",1, 300L));
        return output;
    }

}