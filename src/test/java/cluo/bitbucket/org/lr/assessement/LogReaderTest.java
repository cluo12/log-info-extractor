package cluo.bitbucket.org.lr.assessement;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.io.File;
import java.io.IOException;
import java.util.List;

import cluo.bitbucket.org.lr.assessement.dto.InputRowData;
import cluo.bitbucket.org.lr.assessement.exception.LogReaderException;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;


public class LogReaderTest
{

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void process_returnExpectedRows_givenAValidInputFile() throws Exception
    {
        LogReader logReader = new LogReader(new File("src/test/resources/input/mini.log"));
        List<InputRowData> rows = logReader.process();
        assertEquals("Number of rows is not expected", 5, rows.size());
        assertThat("The 2nd row is not expected", rows.get(1).getPageLabel(), is("Home_Login"));
    }

    @Test
    public void process_throwsIOException_givenInputFileDoesntExist() throws Exception {
        expectedException.expect(IOException.class);

        LogReader logReader = new LogReader(new File("src/test/resources/input/doesntExist.log"));
        logReader.process();
    }

    @Test
    public void process_throwsLogReaderException_givenInputFileHasWrongHeaders() throws Exception {
         expectedException.expect(LogReaderException.class);
         expectedException.expectMessage("Input file has wrong header row, expected headers "+LogReader.HEADER_COUNT);
        LogReader logReader = new LogReader(new File("src/test/resources/input/input-missing-header.log"));
        logReader.process();
    }

    @Test
    public void process_throwsLogReaderException_givenInputFileHasNoLabel() throws Exception
    {
        expectedException.expect(LogReaderException.class);
        expectedException.expectMessage("Input file has no header 'label'" );
        LogReader logReader = new LogReader(new File("src/test/resources/input/input-missing-label.log"));
        logReader.process();
    }

    @Test
    public void process_throwsLogReaderException_givenInputFileHasNoLatency() throws Exception
    {
        expectedException.expect(LogReaderException.class);
        expectedException.expectMessage("Input file has no header 'Latency'");
        LogReader logReader = new LogReader(new File("src/test/resources/input/input-missing-latency.log"));
        logReader.process();
    }

}