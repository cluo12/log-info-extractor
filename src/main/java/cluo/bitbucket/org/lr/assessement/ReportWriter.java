package cluo.bitbucket.org.lr.assessement;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

import cluo.bitbucket.org.lr.assessement.dto.OutputRowData;
import cluo.bitbucket.org.lr.assessement.exception.ReportWriterException;
import lombok.NonNull;

public final class ReportWriter
{
    static final String[] FILE_HEADERS = new String[]{"Label","Count","Average"};
    private static final String NEW_LINE_SEPARATOR = "\n";
    private static final Logger LOG = LoggerFactory.getLogger(ReportWriter.class);

    private File outputFile;

    public ReportWriter(@NonNull File outputFile)
    {
        this.outputFile = outputFile;
    }

    public void write(@NonNull List<OutputRowData> outputData) throws ReportWriterException
    {
        LOG.info("Writing data to {} ....", getOutputFile().getName());

        try (
            FileWriter csvFileWriter = new FileWriter(this.outputFile);
            CSVPrinter csvFilePrinter = new CSVPrinter(csvFileWriter, createCSVFormat())
            ) {
            for(OutputRowData record : outputData)
            {
               csvFilePrinter.printRecord(record.getPageLabel(),record.getCount(), record.getAverageLatency());
            }
        }
        catch(IOException ioException)
        {
            throw new ReportWriterException(ioException.getMessage(), ioException);
        }

    }

    private CSVFormat createCSVFormat()
    {
        return CSVFormat.DEFAULT
                  .withRecordSeparator(NEW_LINE_SEPARATOR)
                  .withHeader(FILE_HEADERS);
    }

    public File getOutputFile()
    {
        return outputFile;
    }
}
