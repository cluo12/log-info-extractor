package cluo.bitbucket.org.lr.assessement;


import org.apache.log4j.Logger;

import java.io.File;
import java.io.IOException;
import java.util.List;

import cluo.bitbucket.org.lr.assessement.dto.InputRowData;
import cluo.bitbucket.org.lr.assessement.dto.OutputRowData;
import cluo.bitbucket.org.lr.assessement.exception.LogReaderException;
import cluo.bitbucket.org.lr.assessement.exception.ReportWriterException;

public final class MainApp
{
    static final String OUTPUT_FILE = "Summary.csv";

    private static final Logger LOG = Logger.getLogger(MainApp.class);

    public static void main(String[] args)
    {
        if(null == args || args.length != 1)
        {
            LOG.info("Usage: cluo.bitbucket.org.lr.assessement.MainApp <input_file.csv>");
            System.exit(-1);
        }
        long start = System.currentTimeMillis();
        try
        {
            final LogReader logReader = new LogReader(new File(args[0]));

            final List<InputRowData> inputRecords = logReader.process();

            final LogAnalyser logAnalyser = new DefaultLogAnalyser();

            final List<OutputRowData> outputRecords = logAnalyser.analyse(inputRecords);

            final ReportWriter reportWriter = new ReportWriter(new File(OUTPUT_FILE));
            reportWriter.write(outputRecords);

            LOG.info(String.format("Time elapsed: %d msec", System.currentTimeMillis() - start));

        }
        catch(LogReaderException | IOException | ReportWriterException appException)
        {
            LOG.error(appException.getMessage(), appException);
        }
    }
}
