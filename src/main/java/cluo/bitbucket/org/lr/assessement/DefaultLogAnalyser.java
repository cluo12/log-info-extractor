package cluo.bitbucket.org.lr.assessement;

import org.apache.commons.lang3.tuple.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import cluo.bitbucket.org.lr.assessement.dto.InputRowData;
import cluo.bitbucket.org.lr.assessement.dto.OutputRowData;
import lombok.NonNull;

public final class DefaultLogAnalyser implements LogAnalyser
{
    private static final Logger LOG = LoggerFactory.getLogger(DefaultLogAnalyser.class);


    @Override
    public List<OutputRowData> analyse(@NonNull List<InputRowData> logRecords)
    {
        LOG.info ("Analysing {} input records ...", logRecords.size());

        if(logRecords.isEmpty())
        {
            return Collections.emptyList();
        }

        Map<String, Pair> countAndLatencySumMap = calculateCountAndAvgLatencyByPageLabel(logRecords);

        List<OutputRowData> outputRecords = new ArrayList<>();

        for(Map.Entry<String, Pair> entry : countAndLatencySumMap.entrySet())
        {
             outputRecords.add(OutputRowData.builder()
                             .pageLabel(entry.getKey())
                             .count((Integer) entry.getValue().getLeft())
                             .averageLatency(getAverageLatency(entry.getValue()))
                             .build());
        }


        return outputRecords;
    }

    private Map<String, Pair> calculateCountAndAvgLatencyByPageLabel(final List<InputRowData> logRecords)
    {

        Map<String, Pair> pageCountAndLatencySumPairMap = new LinkedHashMap<>();

        for(InputRowData logRecord : logRecords)
        {
            if(pageCountAndLatencySumPairMap.containsKey(logRecord.getPageLabel()))
            {
                Pair countAndLatencySumPair = pageCountAndLatencySumPairMap.get(logRecord.getPageLabel());

                pageCountAndLatencySumPairMap.put(logRecord.getPageLabel(),
                      ofPair((Integer) countAndLatencySumPair.getLeft() + 1,
                          (Long) countAndLatencySumPair.getRight() + logRecord.getLatency()));

            } else
            {
                pageCountAndLatencySumPairMap.put(logRecord.getPageLabel(),
                      ofPair( 1, logRecord.getLatency()));

            }
        }

        return pageCountAndLatencySumPairMap;
     }

    private long getAverageLatency(Pair pair)
    {
        return (Long)pair.getRight() / (Integer)pair.getLeft();
    }

     private Pair<Integer,Long> ofPair(int left, long right)
     {
         return Pair.of(left, right);
     }
}