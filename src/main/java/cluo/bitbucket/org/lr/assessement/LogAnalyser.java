package cluo.bitbucket.org.lr.assessement;

import java.util.List;

import cluo.bitbucket.org.lr.assessement.dto.InputRowData;
import cluo.bitbucket.org.lr.assessement.dto.OutputRowData;
import lombok.NonNull;

public interface LogAnalyser
{

    /**
     * Analyse the given log records to find out page count and average latency for each web page
     *
     * @param logRecords The log records. The value can not be null, otherwise a NullPointException
     *                   will be thrown
     * @return A collection of output records i.e. ${@link OutputRowData} or empty list if the given
     *         input is empty
     */

    List<OutputRowData> analyse(@NonNull List<InputRowData> logRecords);
}
