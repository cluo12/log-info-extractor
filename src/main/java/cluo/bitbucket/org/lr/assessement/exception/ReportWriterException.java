package cluo.bitbucket.org.lr.assessement.exception;


public class ReportWriterException extends Exception
{
    public ReportWriterException(String message)
    {
        super(message);
    }

    public ReportWriterException(String message, Throwable cause)
    {
        super(message, cause);
    }
}
