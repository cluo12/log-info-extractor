package cluo.bitbucket.org.lr.assessement.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class InputRowData
{
    private String pageLabel;
    private long latency;
}
