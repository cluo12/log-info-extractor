package cluo.bitbucket.org.lr.assessement;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import cluo.bitbucket.org.lr.assessement.dto.InputRowData;
import cluo.bitbucket.org.lr.assessement.exception.LogReaderException;
import lombok.NonNull;

public final class LogReader
{
    private static final Logger LOG = LoggerFactory.getLogger(LogReader.class);
    public static final int HEADER_COUNT = 16;
    private File logFile;

    public LogReader(@NonNull File logFile)
    {
        this.logFile = logFile;
    }

    /**
     * Process the log file and produce a collection of input records represented by ${@link InputRowData}
     * @return  A collection of InputRowData objects
     * @throws IOException if any IO error occurs
     * @throws LogReaderException if the log file being process is invalid e.g. missing headers or
     *          core fields such as 'label' and 'Latency'
     */
    public List<InputRowData> process() throws IOException, LogReaderException
    {
        LOG.info("Processing file: {} .... ",this.logFile.getCanonicalFile());
        List<InputRowData> rows = new ArrayList<>();
        try (
            Reader fileReader = new FileReader(this.logFile);
            CSVParser csvRecords = CSVFormat.RFC4180.withFirstRecordAsHeader().parse(fileReader)
        ) {
            validateHeaders(csvRecords.getHeaderMap());

            for(CSVRecord csvRecord : csvRecords)
            {
                rows.add(buildInputRowData(csvRecord));
            }
        }

        return rows;
    }

    private void validateHeaders(Map<String, Integer> headerMap) throws LogReaderException
    {
        if (headerMap.size()!= HEADER_COUNT) {
           throw new LogReaderException("Input file has wrong header row, expected headers "+ HEADER_COUNT);
        }

        if (!headerMap.containsKey("label")) {
            throw new LogReaderException("Input file has no header 'label'");
        }

        if (!headerMap.containsKey("Latency")) {
            throw new LogReaderException("Input file has no header 'Latency'");
        }
    }

    private InputRowData buildInputRowData(CSVRecord csvRecord)
    {
        return InputRowData.builder()
                  .pageLabel(csvRecord.get("label").trim())
                  .latency(Long.parseLong(csvRecord.get("Latency").trim()))
                .build();
    }
}
