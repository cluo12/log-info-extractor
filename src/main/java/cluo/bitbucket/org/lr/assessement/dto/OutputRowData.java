package cluo.bitbucket.org.lr.assessement.dto;


import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class OutputRowData
{
    private String pageLabel;
    private int count;
    private long averageLatency;

}
