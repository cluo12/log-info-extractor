package cluo.bitbucket.org.lr.assessement;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import cluo.bitbucket.org.lr.assessement.dto.InputRowData;
import cluo.bitbucket.org.lr.assessement.dto.OutputRowData;
import lombok.NonNull;

import static java.util.stream.Collectors.averagingLong;
import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.reducing;

/**
 * An alternative implementation of LogAnalyser using Stream Collectors.
 *
 *  For my own curiosity, I have also implemented the log analyser using Stream Collectors.
 *  I compared the two implementations (i.e. DefaultLogAnalyser VS LogAnalyserWithStream)
 *  after five runs of each, and then I found out that the old school way is about 48% faster
 *  (i.e. 116 msec VS 225 msec)
 */
public final class LogAnalyserWithStream implements LogAnalyser
{
    private static final Logger LOG = LoggerFactory.getLogger(LogAnalyserWithStream.class);

    public List<OutputRowData> analyse(@NonNull List<InputRowData> logRecords)
    {
        LOG.info ("Analysing {} input records ...", logRecords.size());

        if(logRecords.isEmpty())
        {
            return Collections.emptyList();
        }

        Map<String, Integer> pageCountMap = calculateCountGroupingByPageLabel(logRecords);

        Map<String, Double> pageAvgLatencyMap =
                                    calculateAverageLatencyGroupingByPageLabel(logRecords);

        List<OutputRowData> outputRecords = new ArrayList<>();

        for(Map.Entry<String, Integer> entry : pageCountMap.entrySet())
        {
            outputRecords.add(OutputRowData.builder()
                .pageLabel(entry.getKey())
                .count(entry.getValue())
                .averageLatency(pageAvgLatencyMap.get(entry.getKey()).longValue())
                .build());
        }

        return outputRecords;
    }

    private Map<String, Double> calculateAverageLatencyGroupingByPageLabel(final List<InputRowData> logRecords)
    {
        return logRecords.stream()
            .sorted(Comparator.comparing(InputRowData::getPageLabel))
            .collect(groupingBy(InputRowData::getPageLabel,
                averagingLong(InputRowData::getLatency))
            );
    }

    private Map<String, Integer> calculateCountGroupingByPageLabel(final List<InputRowData> logRecords)
    {
        return logRecords.stream()
            .sorted(Comparator.comparing(InputRowData::getPageLabel))
            .collect(groupingBy(InputRowData::getPageLabel,
                       reducing(0, item -> 1, Integer::sum)));
    }
}
